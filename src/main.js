// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Card from "./components/Card";
import Header from "./components/Header";

Vue.config.productionTip = false

/* eslint-disable no-new */
Vue.component('app-card', Card);
Vue.component('app-header', Header);
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
